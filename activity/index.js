console.log("Hello world");

let number = parseInt(prompt("Enter a number"));
console.log("The number you provided is " + number);

for(number; number > 0; number--){
	if(number <= 50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	}
	if(number % 10 === 0){
		console.log("This number is divisible by 10. Skipping the number.");
	}
	else if(number % 5 === 0){
		console.log(number);
	}
}

function multiplicationTable(){
	for(let a = 1; a < 11; a++)
	{
		console.log("5 x " + a + " = " + (a*5));
	}
}

multiplicationTable();