console.log("Hello World");

function mantraToday(){
	console.log("D-3 bago masilayan si Irene");
}

// While Loop
// A while loops allows us to repeat a task/code while the condition is true.

/*Syntax:
	while(condition){

		task;
		increment/decrement;
	}
*/

// variable for counter, it will set how many time we will be able to repeat our task.
let count = 5;

// As long as out count variable'svalue is not equal to zero, we will be able to repeat our task
// while(count !== 0){

// 	mantraToday();
// 	// If a loop is not able to falsify its condiion, you will be facing an infinite loop
// 	count--;
// }

// As long as count is not equal to zero, show the current count.
// while(count !== 0){

// 	console.log(count);
// 	count--;

// }

// As long as the condition is true, the loop will run

// Do While Loops
// Do While loops are similar to while loops that it allows us to repeat actions/tasks as long as the condition is true. However, with a do while loop, you are able to perform a task at least once even if the condition is not true.

// do {
// 	console.log(count);
// 	count--;
// }	while (count === 0);

// while(count === 0){
// 	console.log(count);
// 	count--;
// }

let counterMini = 20;
while(counterMini !== 0){
	console.log(counterMini);
	counterMini--;
}

// For Loop
// The For Loop is a more flexible version of our while and do while loops.
/*
It consists 3 parts
1. the declaration/initialization of the counter
2, the condition that will be evaluated to determine if the loop will continue
3. the iteration or the incrementation/decrementation need to continue and arrive at a terminating/end condition
*/

// for(let count = 0; count <= 20; count++){
// 	console.log(count);
// }

// Mini Activity

// for(let x = 1; x <= 9; x++){
// 	console.log("the sum of " + "1" + " + " + x + " = " + (x+1));
// }

/*
Sir Teejay solution:

for(let x = 1; x < 10; x++){
	let sum = 1 + x
	console.log("the sum of " + 1 + " + " + x + " = " + sum *);
}
*/

// Continue and Break
/*
	Continue is a keyword that allows the code to go to the next loop without finishing the current code block
*/

for(let counter = 0; counter <= 20; counter++){
	if(counter % 2 === 0){
		continue;
	}
	// when we use the continue keyword, the code block following is disregarded and the next loop is run.
	// whenever the value of the counter is an even number, we skip to the next loop. Therefore, only odd numbers are shown.
	console.log(counter);

	/*
		Break - allows us to end the execution of a loop
	*/
	if(counter === 1){
		break;
	}
}

for(let divisibleBy5 = 0; divisibleBy5 <= 100; divisibleBy5++){
	if(divisibleBy5 % 5 === 0){
	console.log(divisibleBy5);
	}
}